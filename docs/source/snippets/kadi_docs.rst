.. note::
    Note that this library targets the stable version ``v1`` of the HTTP API of
    Kadi4Mat. Detailed information about the API itself, including all endpoints and
    their parameters, can be found in the developer `documentation
    <https://kadi.readthedocs.io/en/stable/#httpapi>`__ of Kadi4Mat.
