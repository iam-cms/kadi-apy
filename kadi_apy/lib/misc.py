# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import re

from kadi_apy.lib.commons import RequestMixin
from kadi_apy.lib.commons import VerboseMixin
from kadi_apy.lib.imports import import_eln
from kadi_apy.lib.imports import import_json_schema
from kadi_apy.lib.imports import import_shacl
from kadi_apy.lib.utils import generate_identifier
from kadi_apy.lib.utils import get_resource_type
from kadi_apy.lib.utils import paginate


def _fetch_records(manager, collection_id):
    """Fetch all records in the collection and return their identifiers."""
    collection = manager.collection(id=collection_id)
    # Fetch all the records in a collection and store record identifiers in a list
    record_identifiers = []

    def process_page(page):
        response = collection.get_records(page=page, per_page=100).json()
        record_identifiers.extend(item["identifier"] for item in response["items"])
        return response

    paginate(process_page)
    return record_identifiers, collection


def _create_modified_collection(manager, collection, suffix):
    """Create a modified collection with a validated suffix."""
    collection_identifier = collection.meta["identifier"]
    modified_collection_identifier = _append_suffix(collection_identifier, suffix)
    modified_collection = manager.collection(
        title=collection.meta["title"],
        description=collection.meta["description"],
        identifier=modified_collection_identifier,
        tags=collection.meta["tags"],
        visibility=collection.meta["visibility"],
        create=True,
    )
    return modified_collection


def _process_records(
    manager, collection, record_identifiers, suffix, modified_collection
):
    """Create new records while preserving the linkage feature"""
    my_records = {}

    for identifier in record_identifiers:
        modified_identifier = _append_suffix(identifier, suffix)
        original_record = manager.record(identifier=identifier)

        new_record = manager.record(
            title=original_record.meta["title"],
            identifier=modified_identifier,
            type=original_record.meta["type"],
            description=original_record.meta["description"],
            extras=original_record.meta["extras"],
            tags=original_record.meta["tags"],
            visibility=original_record.meta["visibility"],
            license=original_record.meta["license"],
            create=True,
        )

        my_records[identifier] = {
            "old_record": original_record,
            "new_record": new_record,
        }

        # Add the new record to the modified collection
        modified_collection.add_record_link(record_id=new_record.id)

    # Process links only after all intended records are created
    for records in my_records.values():
        old_record = records["old_record"]
        new_record = records["new_record"]

        def process_links(page, old_record=old_record, new_record=new_record):
            """Paginate links for a specific old_record"""
            response = old_record.get_record_links(
                direction="out", per_page=100, page=page
            ).json()
            for link in response["items"]:
                linked_identifier = link["record_to"]["identifier"]
                if linked_identifier in my_records:
                    new_record_to = my_records[linked_identifier]["new_record"]
                    new_record.link_record(
                        record_to=new_record_to.id, name=link["name"]
                    )
            return response

        paginate(process_links)

    # Link the modified collection to the parent collection
    collection.add_collection_link(collection_id=modified_collection.id)


def _append_suffix(identifier, suffix):
    max_combined_length = 50
    max_identifier_length = 40

    suffix = suffix.strip()
    suffix = generate_identifier(suffix)

    # Remove leading and trailing dashes or underscores
    suffix = re.sub(r"^[-_]+|[-_]+$", "", suffix)
    identifier = identifier[:max_identifier_length]

    return f"{identifier}_{suffix}"[:max_combined_length]


class Miscellaneous(RequestMixin, VerboseMixin):
    """Model to handle miscellaneous functionality.

    :param manager: Manager to use for all API requests.
    :type manager: KadiManager
    """

    def get_deleted_resources(self, **params):
        r"""Get a list of deleted resources in the trash. Supports pagination.

        :param \**params: Additional query parameters.
        :return: The response object.
        """

        endpoint = "/trash"
        return self._get(endpoint, params=params)

    def restore(self, item, item_id):
        """Restore an item from the trash.

        :param item: The resource type defined either as string or class.
        :param item_id: The ID of the item to restore.
        :type item_id: int
        :return: The response object.
        """

        if isinstance(item, str):
            item = get_resource_type(item)

        endpoint = f"{item.base_path}/{item_id}/restore"
        return self._post(endpoint)

    def purge(self, item, item_id):
        """Purge an item from the trash.

        :param item: The resource type defined either as string or class.
        :param item_id: The ID of the item to restore.
        :type item_id: int
        :return: The response object.
        """

        if isinstance(item, str):
            item = get_resource_type(item)

        endpoint = f"{item.base_path}/{item_id}/purge"
        return self._post(endpoint)

    def get_licenses(self, **params):
        r"""Get a list of available licenses.  Supports pagination.

        :param \**params: Additional query parameters.
        :return: The response object.
        """

        endpoint = "/licenses"
        return self._get(endpoint, params=params)

    def get_kadi_info(self):
        """Get information about the Kadi instance.

        :return: The response object.
        """

        endpoint = "/info"
        return self._get(endpoint)

    def get_roles(self):
        """Get all possible roles and corresponding permissions of all resources.

        :return: The response object.
        """

        endpoint = "/roles"
        return self._get(endpoint)

    def get_tags(self, **params):
        r"""Get a list of all tags. Supports pagination.

        :param \**params: Additional query parameters.
        :return: The response object.
        """

        endpoint = "/tags"
        return self._get(endpoint, params=params)

    def import_eln(self, file_path):
        """Import an RO-Crate file following the "ELN" file specification.

        :param file_path: The path of the file.
        :type file_path: str
        :raises KadiAPYInputError: If the structure of the RO-Crate is not valid.
        :raises KadiAPYRequestError: If any request was not successful while importing
            the data and metadata.
        """
        import_eln(self.manager, file_path)

    def import_json_schema(self, file_path, template_type="extras"):
        """Import JSON Schema file and create a template.

        Note that only JSON Schema draft 2020-12 is fully supported, but older schemas
        might still work.

        :param file_path: The path of the file.
        :type file_path: str
        :param template_type: Type of the template. Can either be ``"record"`` or
            ``"extras"``.
        :type template_type: str
        :raises KadiAPYInputError: If the structure of the Schema is not valid.
        :raises KadiAPYRequestError: If any request was not successful while importing
            the metadata.
        """
        import_json_schema(self.manager, file_path, template_type)

    def import_shacl(self, file_path, template_type="extras"):
        """Import SHACL Shapes file and create a template.

        :param file_path: The path of the file.
        :type file_path: str
        :param template_type: Type of the template. Can either be ``"record"`` or
            ``"extras"``.
        :type template_type: str
        :raises KadiAPYInputError: If the structure of the Shapes is not valid.
        :raises KadiAPYRequestError: If any request was not successful while importing
            the metadata.
        """
        import_shacl(self.manager, file_path, template_type)

    def reuse_collection(self, collection_id, suffix):
        """Create a modified version of a collection with updated identifiers.

        This method creates a new collection by duplicating all records from an existing
        collection and appending the given suffix to their identifiers. It ensures that
        record links are preserved within the modified collection.

        :param collection_id: The ID of the collection to be reused.
        :param suffix: A string to append to each record and collection identifier.
        :raises KadiAPYRequestError: If any request was not successful during
            the duplication process.
        """
        record_identifiers, collection = _fetch_records(self.manager, collection_id)
        modified_collection = _create_modified_collection(
            self.manager,
            collection,
            suffix,
        )
        _process_records(
            self.manager,
            collection,
            record_identifiers,
            suffix,
            modified_collection,
        )
